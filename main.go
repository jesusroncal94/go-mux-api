package main

import (
	"go-mux-api/routes"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	// Register main routes
	routes.SetMainRoutes(r)

	// Bind to a port and pass our router in
	server := http.Server{
		Addr:    ":8000",
		Handler: r,
	}
	log.Println("Running on port 8000")
	log.Println(server.ListenAndServe())
}
