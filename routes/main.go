package routes

import (
	"net/http"

	"github.com/gorilla/mux"
)

func SetMainRoutes(r *mux.Router) {
	r.HandleFunc("/", RootHandler)
	r.HandleFunc("/api/v1", APIRootHandler)
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Golang Mux App running successfully!"))
}

func APIRootHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Golang Mux API running successfully!"))
}
